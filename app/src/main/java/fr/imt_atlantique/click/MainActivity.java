package fr.imt_atlantique.click;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int nbrClick = 0;
    private TextView textViewNbrClick;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewNbrClick = findViewById(R.id.textViewClick);

        if (savedInstanceState != null) {
            nbrClick = savedInstanceState.getInt("nbrClick");
        }

        textViewNbrClick.setText(String.valueOf(nbrClick));

        findViewById(android.R.id.content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nbrClick++;
                textViewNbrClick.setText(String.valueOf(nbrClick));
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("nbrClick", nbrClick);
        super.onSaveInstanceState(outState);
    }
}